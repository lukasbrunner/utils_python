#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Various python utility functions.
"""
import os
import logging
import numpy as np
import __main__ as main
from configparser import ConfigParser
from munch import munchify

logger = logging.getLogger(__name__)


format_ = '%(asctime)s - %(levelname)s - %(funcName)s() %(lineno)s: %(message)s'
def set_logger(**kwargs):
    """Set up a basic logger"""
    defaults = dict(
        format=format_,
        level=logging.INFO,
        filename=None,
        filemode='w')
    defaults.update(kwargs)
    logging.basicConfig(**defaults)


def log_parser(args):
    def _print(ll, max_len=10):
        if isinstance(ll, (list, np.ndarray)):
            if len(ll) < max_len:
                return ', '.join(map(str, ll))
            else:
                return '{}, {}, ..., {}, {} (n={})'.format(*ll[:2], *ll[-2:], len(ll))
        return ll

    if type(args).__module__ == 'munch':
        logmsg = 'Read configuration file: \n\n'
        for ii in sorted(args.keys()):
            logmsg += '  {}: {}\n'.format(ii, _print(args[ii]))
    else:
        logmsg = 'Read parser input: \n\n'
        for ii, jj in sorted(vars(args).items()):
            logmsg += '  {}: {}\n'.format(ii, _print(jj))
    logger.info(logmsg)


def read_config(cname='DEFAULT', path='config.ini', separator=','):
    """Reads a config.ini file and returns a object containing
    its values.

    Parameters:
    - cname=None (str, optional): Name of the configuration to use.
    - path=None (str, optional): Path of the configuration file. Defaults to
      ./config.ini of the main file.
    - separator=',' (str, optional): Strings containing separator will be
      split into list. Set to None to disable this behavior.

    Returns:
    object

    The returned object contains all values as attributes plus one additional
    attribute config which contains 'cname'.

    Additional information:
    The ConfigParser method only returns strings by default. For convenience
    numerical expressions with a dot (e.g., 0.) will be converted to float and
    numerical expressions without a dot (e.g., 0) will be converted to int.
    To force a string of a number use explicit quotes (e.g., "0" or '0') in
    the .ini file. The strings 'False', 'True', and 'None' will be converted
    to their logical equivalents.  Strings containing the 'separator' string
    will be converted to list."""

    def _convert(str_):
        str_ = str_.strip()
        try:
            return int(str_)
        except ValueError:
            try:
                return float(str_)
            except ValueError:
                if str_ == 'True':
                    return True
                elif str_ == 'False':
                    return False
                elif str_ == 'None':
                    return None
                elif len(str_) == 0:  # NOTE: ignore empty strings in list
                    raise ValueError
                else:  # use '123' to force a string
                    return str(str_.replace("'", "").replace('"', ''))

    if not path.endswith('.ini'):
        path += '.ini'
    basepath = os.path.dirname(os.path.realpath(main.__file__))
    fullpath = os.path.join(basepath, path)

    if not os.path.isfile(fullpath):
        raise IOError('{} is not a valid filename'.format(fullpath))

    config = ConfigParser()
    config.read(fullpath)
    cc = munchify(dict(config[cname]))
    for key in cc.keys():
        if separator is not None and separator in cc[key]:
            cc[key] = cc[key].split(separator)
            for idx, _ in enumerate(cc[key]):
                try:
                    cc[key][idx] = _convert(cc[key][idx])
                except ValueError:  # NOTE: ignore empty strings in list
                    del cc[key][idx]
        else:
            cc[key] = _convert(cc[key])

    cc.config = cname  # also contain the name of the configuration
    cc.config_path = fullpath  # and path of the config file
    return cc
