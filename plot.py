#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-09-27 15:00:18 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import numpy as np
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt



def get_color_list(nn=None, names=False):
    """A convenience function returning ~40 user defined nicely-looking colors.

    Parameters:
    - nn=None (int, optional): Number of colors to return. If less than
      available they will be stretched over the full range (i.e., some
      colors 'in the middle' will be missing).
    - names=False (bool, optional): Return name strings instead of colors.

    Returns:
    seaborn.color_palette OR list of str"""

    color_names = [
        'darkred',
        'firebrick',
        'crimson',
        'orangered',
        'indianred',
        'tomato',
        'salmon',
        'lightsalmon',
        'darksalmon',
        'peru',
        'goldenrod',
        'rosybrown',
        'tan',
        'beige',
        'palegoldenrod',
        'khaki',
        'darkkhaki',
        'olive',
        'olivedrab',
        'forestgreen',
        'darkgreen',
        'mediumseagreen',
        'aquamarine',
        'lightseagreen',
        'mediumturquoise',
        'paleturquoise',
        'lightblue',
        'lightskyblue',
        'dodgerblue',
        'steelblue',
        'royalblue',
        'darkslateblue',
        'rebeccapurple',
        'blueviolet',
        'darkorchid',
        'purple',
        'orchid',
        'magenta',
        'hotpink',
        'lightpink',
        'palevioletred',
        'mediumvioletred',
    ]

    if nn is not None and nn < len(color_names):
        np.random.seed(0)
        idx = np.random.choice(range(len(color_names)), nn, replace=False)
        idx.sort()
    elif nn is not None and nn > len(color_names):
        raise NotImplementedError
    else:
        idx = np.arange(len(color_names))

    if names:
        return list(np.array(color_names)[idx])
    return sns.color_palette(np.array(color_names)[idx])



if __name__ == '__main__':
    fig, ax = plt.subplots()
    for ii, color in enumerate(get_color_list()):
        ax.hlines(ii, 0, 1, colors=color, lw=3)
    plt.show()
