#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2018-10-19 14:30:28 lukbrunn>

(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Utility functions implementing mathematical operations.

References:
- variance
https://stackoverflow.com/questions/2413522/weighted-standard-deviation-in-numpy#2415343
https://en.wikipedia.org/wiki/Weighted_arithmetic_mean#Weighted_sample_variance

- percentile
https://stackoverflow.com/questions/21844024/weighted-percentile-using-numpy#29677616
"""
import numpy as np


def variance(data, weights=None, biased=False):
    """Calculates the (biased/unbiased) weighted variance.

    Parameters:
    - data (np.array): Array of data (N,)
    - weights=None (np.array, optional): Array of weights (N,)
    - biased=False (bool, optional): Calculate the biased or unbiased value.

    Returns:
    float (see also Note)

    Note (masked input/output behavior):
    The behavior of variance is equivalent to np.ma.var: if the result is not
    masked (i.e., if NOT all input values are masked) it will return a simple
    float. If all input values are masked it will return a
    numpy.ma.core.MaskedConstant"""
    was_masked = isinstance(data, np.ma.core.MaskedArray)
    data = np.ma.masked_invalid(data)

    if weights is None:
        weights = np.ones_like(data)
    weights = np.ma.array(weights, mask=data.mask)

    av = np.ma.average(data, weights=weights)
    if biased:
        var = np.ma.average(np.ma.subtract(data, av)**2, weights=weights)
    else:
        v1, v2 = np.ma.sum(weights), np.ma.sum(weights**2)
        var = np.ma.divide(
            np.ma.sum(np.ma.multiply(weights, np.subtract(data, av)**2)),
            (v1 - (v2/v1)))

    if isinstance(var, float):  # if result is not masked
        return var
    elif was_masked:
        return var  # if invalid & input was masked return masked
    return np.nan  # else return nan


def quantile(data, quantiles, weights=None, interpolation='linear',
             old_style=False):
    """Calculates weighted quantiles.

    Parameters:
    - data (np.array): Array of data (N,)
    - quantiles (np.array): Array of quantiles (M,) in [0, 1]
    - weights=None (np.array, optional): Array of weights (N,)
    - interpolation='linear' (str, optional): String giving the interpolation
      method (equivalent to np.percentile). "This optional parameter specifies
      the interpolation method to use when the desired quantile lies between
      two data points." One of (with i < j):
      * linear: i + (j - i) * fraction where fraction is the fractional part
        of the index surrounded by i and j
      * lower: i  NOTE: might lead to unexpected results for integers (see
        tests/test_math.test_quantile_interpolation)
      * higher: j  NOTE: might lead to unexpected results for integers
      * nearest: i or j whichever is nearest
      * midpoint: (i + j) / 2. TODO: not yet implemented!
    - old_style=False (bool, optional): If True, will correct output to be
      consistent with np.percentile.

    Returns:
    np.array of shape (M,)"""
    data = np.array(data)
    quantiles = np.array(quantiles)
    if np.any(np.isnan(data)):
        errmsg = ' '.join([
            'This function is not tested with missing data! Comment this test',
            'if you want to use it anyway.'])
        raise ValueError(errmsg)
    if data.ndim != 1:
        errmsg = 'data should have shape (N,) not {}'.format(data.shape)
        raise ValueError(errmsg)
    if np.any(quantiles < 0.) or np.any(quantiles > 1.):
        errmsg = 'quantiles should be in [0, 1] not {}'.format(quantiles)
        raise ValueError(errmsg)
    if weights is None:
        weights = np.ones_like(data)
    else:
        weights = np.array(weights)
        if data.shape != weights.shape:
            errmsg = ' '.join([
                'weights need to have the same shape as data ',
                '({} != {})'.format(weights.shape, data.shape)])
            raise ValueError(errmsg)
        # remove values with weights zero
        idx = np.where(weights==0)[0]
        weights = np.delete(weights, idx)
        data = np.delete(data, idx)

    sorter = np.argsort(data)
    data = data[sorter]
    weights = weights[sorter]

    weighted_quantiles = np.cumsum(weights) - .5*weights

    if old_style:  # consistent with np.percentile
        weighted_quantiles -= weighted_quantiles[0]
        weighted_quantiles /= weighted_quantiles[-1]
    else:  # more correct (see reference for a discussion)
        weighted_quantiles /= np.sum(weights)

    results = np.interp(quantiles, weighted_quantiles, data)

    if interpolation == 'linear':
        return results
    elif interpolation == 'lower':
        if isinstance(results, float):
            return data[data<=results][-1]
        return np.array([data[data<=rr][-1] for rr in results])
    elif interpolation == 'higher':
        if isinstance(results, float):
            return data[data>=results][0]
        return np.array([data[data>=rr][0] for rr in results])
    elif interpolation == 'nearest':
        if isinstance(results, float):
            return data[np.argmin(np.abs(data - results))]
        return np.array([data[np.argmin(np.abs(data - rr))] for rr in results])
    elif interpolation == 'midpoint':
        raise NotImplementedError
    else:
        errmsg = ' '.join([
            'interpolation has to be one of [linear | lower | higher |',
            'nearest | midpoint] and not {}'.format(interpolation)])
        raise ValueError(errmsg)
