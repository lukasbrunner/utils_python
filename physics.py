#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Utility functions with dependence on physical units,
geophysical parameters, etc.

"""
import numpy as np


def area_weighted_mean(data, lat, lon):
    """Calculates an area-weighted average of data depending on latitude [and
    longitude] and handles missing values correctly.

    Parameters:
    - data (np.array): Data to be averaged, shape has to be (..., lat[, lon]).
    - lat (np.array): Array giving the latitude values.
    - lon (np.array): Array giving the longitude values (only used for
      consistency checks). lon can be None in which case data is considered
      as not containing the longitude dimension (hence latitude has to be the
      last dimension).

    Returns:
    Array of shape (...,) or float"""
    # input testing & pre-processing
    was_masked = False
    if isinstance(data, np.ma.core.MaskedArray):
        was_masked = True
    else:  # in case it a Python list
        data = np.array(data)
    lat = np.array(lat)
    assert len(lat.shape) == 1, 'lat has to be a 1D array'
    assert ((-90 <= lat) & (lat <= 90)).all(), 'lat has to be in [-90, 90]!'
    if lon is None:
        assert data.shape[-1] == len(lat), 'Axis -1 of data has to match lat!'
        data, lon = data.reshape(data.shape + (1,)), np.array([])
    else:
        lon  = np.array(lon)
        assert len(lon.shape) == 1, 'lon has to be a 1D array'
        assert data.shape[-1] == len(lon), 'Axis -1 of data has to match lon!'
        assert data.shape[-2] == len(lat), 'Axis -2 of data has to match lat!'
        errmsg = 'lon has to be in [-180, 180] or [0, 360]!'
        assert (((-180 <= lon) & (lon <= 180)).all() or
                ((0 <= lon) & (lon <= 360)).all()), errmsg

    # create latitude weights and tile them to all longitudes, then flatten
    w_lat = np.cos(np.radians(lat))
    weights = np.tile(w_lat, (data.shape[-1], 1)).swapaxes(0, 1).ravel()
    # flatten lat-lon dimensions, mask missing values, average
    data_flat = np.ma.masked_invalid(data.reshape(data.shape[:-2] + (-1,)))
    mean = np.ma.average(data_flat, axis=-1, weights=weights)

    # NOTE: if data is a single value and not invalid it will be a normal
    # float at this point and no longer masked!
    if isinstance(mean, float):
        return mean
    elif was_masked:  # if input was masked array also return a masked array
        return mean
    return mean.filled(fill_value=np.nan)


# based on the haversine package:
#  https://github.com/mapado/haversine
AVG_EARTH_RADIUS = 6371  # in km
def haversine_grid(point_ref, lats, lons, decimals=7, indexing='ij'):
    """
    Calculate the great-circle distance bewteen two points on the Earth surface.

    Parameters:
    - point_ref (tuple): (lat, lon) reference point
    - lats (array): Array with latitude values
    - lons (array): Array with longitude values
    - decimals=7 (int, optional): Round output to number of decimals. Note:
      setting this too high might lead to NANs if (lats, lons) contains
      (clat, clon) due to numerics
    - indexing='ij' (str, optional): passed on to np.meshgrid

    Returns:
    np.meshgrid of distances for each point defined by (lats, lons) in km
    """
    point_ref = map(np.math.radians, point_ref)
    lats = np.array(list(map(np.math.radians, lats)))
    lons = np.array(list(map(np.math.radians, lons)))
    clat, clon = point_ref

    d_lat = lats - clat
    d_lon = lons - clon

    d_lat, d_lon = np.meshgrid(d_lat, d_lon, indexing=indexing)
    lats, _ = np.meshgrid(lats, lons, indexing=indexing)

    dd = np.around(
        np.sin(d_lat*.5)**2 + np.cos(clat) * np.cos(lats) * np.sin(d_lon*.5)**2,
        decimals)
    return np.around(2 * AVG_EARTH_RADIUS * np.arcsin(np.sqrt(dd)), decimals)
