# Python utility functions

This package contains a collection of Python utility functions. You can contribute by adding additional utility functions to one of the existing script files or by creating a new file. Please follow the following approach:

- Run <code>git pull</code> to make sure you have the latest version
- Check if your function fits into on of the existing categories:
  - **decorators.py**: A collection of decorator functions.
  - **get_filenames.py**: Contains only the Filenames class.
  - **math.py**: Functions implementing non-standard mathematical calculations such as a weighted variance.
  - **physics.py**: A collection of functions with dependence on physical units, geophysical parameters, etc.
  - **plot.py**: A collection of utility functions related to plotting.
  - **run_parallel.py**: A collection of convenience functions used for multiprocessing.
  - **utils.py**: A collection of general utility functions not fitting in any other category.
  - **xarray.py**: A collection of utility functions building on the xarray class.
- Add the function to the collection.
- **Add a test to the tests/ directory and make sure it passes.**
- Run <code>git add your_function.py tests/test_your_function.py; git commit; git push</code>
