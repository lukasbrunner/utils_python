#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2018 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A collection of utility functions for xarray.
"""
import os
import logging
import subprocess
import datetime
import regionmask
import numpy as np
import xarray as xr
import __main__ as main

from .decorators import vectorize
from .physics import area_weighted_mean as area_weighted_mean_data

logger = logging.getLogger(__name__)


def get_variable_name(ds):
    """Try to get the main variable from a dataset.

    This function tries to get the main variable from a dataset by checking the
    following properties:
    - the variable must NOT be a coordinate variable
    - the variable name must NOT contain '_bounds' or '_bnds'
    - There MUST be exactly one variable fulfilling the above

    Parameters
    ----------
    ds : xarray.Dataset
        Dataset with only one main variable, e.g., tas (time, lat, lon)
    """

    varns = [varn for varn in set(ds.variables).difference(ds.coords)
             if '_bounds' not in varn and '_bnds' not in varn]
    if len(varns) != 1:
        errmsg = 'Unable to select a single variable from {}'.format(
            ', '.join(varns))
        raise ValueError(errmsg)
    return varns[0]


# merged from 'blocking'
def get_longitude_name(ds):
    """Get the name of the longitude dimension by CF unit"""
    lonn = []
    if isinstance(ds, xr.core.dataarray.DataArray):
        ds = ds.to_dataset()
    for dimn in ds.dims.keys():
        if ('units' in ds[dimn].attrs and
            ds[dimn].attrs['units'] in ['degree_east', 'degrees_east']):
            lonn.append(dimn)
    if len(lonn) == 1:
        return lonn[0]
    elif len(lonn) > 1:
        errmsg = 'More than one longitude coordinate found by unit.'
    else:
        errmsg = 'Longitude could not be identified by unit.'
    raise ValueError(errmsg)


# merged from 'blocking'
def get_latitude_name(ds):
    """Get the name of the latitude dimension by CF unit"""
    latn = []
    if isinstance(ds, xr.core.dataarray.DataArray):
        ds = ds.to_dataset()
    for dimn in ds.dims.keys():
        if ('units' in ds[dimn].attrs and
            ds[dimn].attrs['units'] in ['degree_north', 'degrees_north']):
            latn.append(dimn)
    if len(latn) == 1:
        return latn[0]
    elif len(latn) > 1:
        errmsg = 'More than one latitude coordinate found by unit.'
    else:
        errmsg = 'Latitude could not be identified by unit.'
    raise ValueError(errmsg)


def flip_antimeridian(ds, to='Pacific', lonn=None):
    """
    Flip the antimeridian (i.e. longitude discontinuity) between Europe
    (i.e., [0, 360)) and the Pacific (i.e., [-180, 180)).

    Parameters:
    - ds (xarray.Dataset or .DataArray): Has to contain a single longitude
      dimension.
    - to='Pacific' (str, optional): Flip antimeridian to one of
      * 'Europe': Longitude will be in [0, 360)
      * 'Pacific': Longitude will be in [-180, 180)
    - lonn=None (str, optional): Name of the longitude dimension. If None it
      will be inferred by the CF convention standard longitude unit.

    Returns:
    same type as input ds
    """
    if lonn is None:
        lonn = get_longitude_name(ds)
    elif lonn not in ds.dims:
        errmsg = '{} not found in Dataset!'.format(lonn)
        raise ValueError(errmsg)

    if to.lower() == 'europe' and ds[lonn].min() >= 0:
        return ds  # already correct, do nothing
    elif to.lower() == 'pacific' and ds[lonn].max() < 180:
        return ds  # already correct, do nothing
    elif to.lower() == 'europe':
        ds = ds.assign_coords(**{lonn: (ds.lon % 360)})
    elif to.lower() == 'pacific':
        ds = ds.assign_coords(**{lonn: (((ds.lon + 180) % 360) - 180)})
    else:
        errmsg = 'to has to be one of [Europe | Pacific] not {}'.format(to)
        raise ValueError(errmsg)

    was_da = isinstance(ds, xr.core.dataarray.DataArray)
    if was_da:
        da_varn = ds.name
        ds = ds.to_dataset()

    idx = np.argmin(ds[lonn].data)
    varns = [varn for varn in ds.variables if lonn in ds[varn].dims]
    for varn in varns:
        if xr.__version__ > '0.10.8':
            ds[varn] = ds[varn].roll(**{lonn: -idx}, roll_coords=False)
        else:
            ds[varn] = ds[varn].roll(**{lonn: -idx})

    if was_da:
        return ds[da_varn]
    return ds


# TODO: untested
# merged from 'blocking'
def sort_latitude(ds, ascending=True):
    """Sort the latitudes"""
    latn = get_latitude_name(ds)
    is_ascending = np.all(ds[latn][1:] - ds[latn][:-1] > 0.)
    if ascending == is_ascending:  # already correct, do nothing
        return ds
    else:
        return ds.sortby(latn, ascending=ascending)


# TODO: untested
def standardize_time(ds, inplace=False, timen='time', **kwargs):
    """Standardize time values

    Due to time averaging some models have slightly different time values,
    such as 2000-01-01 12:00:00.00 and 2000-01-01 13:00:00.00 in monthly
    aggregated data. This prevents merging with xarray.concat() since
    they will be interpreted as different times. This function does some
    checks and standardizes the time to the given values.

    Parameters
    ----------
    ds : {xarray.Dataset, xarray.DataArray}
        Input dataset, needs to contain the time dimension.
    inplace : bool, optional
        If false return a copy with times standardized.
    timen : str, optional
        The time dimension name
    **kwargs : any combination of valid cftime.datetime key value pairs, optional
        If not set then standard values will be used depending on the time
        resolution of ds. If set only keys given in kwargs will be
        standardized. If kwargs contains a key which would set a value larger
        or equal to the time resolution of ds a ValueError will be raised
        (e.g., setting day=15 for daily data will fail since it would
        potentially lead to ambiguous results).

    Returns
    -------
    ds_standardized : same type as ds
        A dataset where the time dimension values have been standardized.

    Defaults
    --------
    The default values correspond to the output of xr.cftime_range and
    pandas.date_range, which is December 31st midnight, i.e.,
    - annual: yyyy-12-31 00:00:00
    - monthly: yyyy-mm-31 00:00:00
    - daily: yyyy-mm-dd 00:00:00
    - sub-daily: return untouched

    Examples
    --------
    >> ds = xr.Dataset(coords={'time_name': xr.cftime_range(
           start='2000-01-01T13:15:30',
           end='2000-01-2T13:15:30',
           freq='1D')})
    >> ds['time_name']
    <xarray.DataArray 'time_name' (time_name: 2)>
    array([cftime.DatetimeProlepticGregorian(2000, 1, 1, 13, 15, 30, 0, -1, 1),
           cftime.DatetimeProlepticGregorian(2000, 1, 2, 13, 15, 30, 0, -1, 1)],
          dtype=object)
    >> standardize_time(ds, timen='time_name').time_name
    <xarray.DataArray 'time_name' (time_name: 2)>
    array([cftime.DatetimeProlepticGregorian(2000, 1, 1, 0, 0, 0, 0, -1, 1),
           cftime.DatetimeProlepticGregorian(2000, 1, 2, 0, 0, 0, 0, -1, 1)])
    >> standardize_time(ds, timen='time_name', hour=12).time_name  # only hour, rest untouched
    <xarray.DataArray 'time_name' (time_name: 2)>
    array([cftime.DatetimeProlepticGregorian(2000, 1, 1, 12, 15, 30, 0, -1, 1),
           cftime.DatetimeProlepticGregorian(2000, 1, 2, 12, 15, 30, 0, -1, 1)])
    """
    dtimes =ds[timen].data[1:]-ds[timen].data[:-1]

    if np.any([dtime.days < 1 for dtime in dtimes]):  # resolution smaller 1 day
        logmsg = 'Time resolution smaller 1 day, standardizing not possible'
        logger.warning(logmsg)
        if not inplace:
            return ds
        return None

    if np.all([dtime.days >= 365 for dtime in dtimes]):  # assume annual
        if len(kwargs) == 0:  # if no kwargs are give use default
            kwargs = dict(
                month=12,
                day=31,
                hour=0,
                minute=0,
                second=0,
                microsecond=0)
        else:  # check if user input is valid
            allowed_keys = ['month', 'day', 'hour', 'minute', 'second', 'microsecond']
            if not np.all([key in allowed_keys for key in kwargs.keys()]):
                errmsg = 'Can not standardize {} for resolution > monthly'.format(
                    ', '.join(set(kwargs.keys()).difference(allowed_keys)))
                raise ValueError(errmsg)
    elif np.all([dtime.days >= 28 for dtime in dtimes]):  # assume monthly
        if len(kwargs) == 0:
            kwargs = dict(
                day=31,
                hour=0,
                minute=0,
                second=0,
                microsecond=0)
        else:
            allowed_keys = ['day', 'hour', 'minute', 'second', 'microsecond']
            if not np.all([key in allowed_keys for key in kwargs.keys()]):
                errmsg = 'Can not standardize {} for resolution > monthly'.format(
                    ', '.join(set(kwargs.keys()).difference(allowed_keys)))
                raise ValueError(errmsg)
    else:  # assume daily or sub-monthly resolution > 1 day
        if len(kwargs) == 0:  # if no kwargs are give use standard settings
            kwargs = dict(
                hour=0,
                minute=0,
                second=0,
                microsecond=0)
        else:
            allowed_keys = ['hour', 'minute', 'second', 'microsecond']
            if not np.all([key in allowed_keys for key in kwargs.keys()]):
                errmsg = 'Can not standardize {} for daily resolution'.format(
                    ', '.join(set(kwargs.keys()).difference(allowed_keys)))
                raise ValueError(errmsg)

    if not inplace:
        ds = ds.copy()

    ds[timen].data = [time.replace(**kwargs) for time in ds[timen].data]

    if not inplace:
        return ds


# TODO: untested
# merged from 'blocking'
def standardize_dimensions(ds, latn='lat', lonn='lon', anitmeridian='Pacific',
                           lat_ascending=True, time={}):
    """Standardize the dimensions of a ds to the given values.
    All parameters are optional and can be set to None to by skipped.

    Parameters
    ----------
    ds : {xarray.Dataset, xarray.DataArray}
        Input dataset, needs to contain latitude and longitude dimensions.
    latn : str, optional
        Name of the new latitude dimension.
    lonn : str, optional
        Name of the new longitude dimension.
    antimeridian : {'Pacific', 'Europe'}, optional
        New location of the antimeridian.
    lat_ascending : bool, optional
        New order of the latitude dimension.
    time : dict, optional
        Passed to standardize_time (see docstring for more information)

    Returns
    -------
    ds_standardized : same type as ds
        A dataset where some dimensions have been standardized.
    """
    latn_old = get_latitude_name(ds)
    lonn_old = get_longitude_name(ds)

    if latn is not None:
        ds = ds.rename(**{latn_old: latn})
    if lonn is not None:
        ds = ds.rename(**{lonn_old: lonn})
    if antimeridian is not None:
        ds = flip_antimeridian(ds, to=anitmeridian)
    if lat_ascending is not None:
        ds = sort_latitude(ds, ascending=lat_ascending)
    if time is not None:
        ds = standardize_time(ds, **time)
    return ds


def area_weighted_mean(ds, latn=None, lonn=None, keep_attrs=True):
    if latn is None:
        latn = get_latitude_name(ds)
    if lonn is None:
        lonn = get_longitude_name(ds)

    was_da = isinstance(ds, xr.core.dataarray.DataArray)
    if was_da:
        da_varn = ds.name
        ds = ds.to_dataset()

    ds_mean = ds.mean((latn, lonn), keep_attrs=keep_attrs)  # create this just to fill
    for varn in set(ds.variables).difference(ds.dims):
        if latn in ds[varn].dims and lonn in ds[varn].dims:
            var = ds[varn].data
            axis_lat = ds[varn].dims.index(latn)
            axis_lon = ds[varn].dims.index(lonn)
            var = np.moveaxis(var, (axis_lat, axis_lon), (-2, -1))
            mean = area_weighted_mean_data(var, ds[latn].data, ds[lonn].data)
        elif latn in ds[varn].dims:
            var = ds[varn].data
            axis_lat = ds[varn].dims.index(latn)
            var = np.moveaxis(var, axis_lat, -1)
            mean = area_weighted_mean_data(var, ds[latn].data, lon_in_data=False)
        elif lonn in ds[varn].dims:
            mean = ds[varn].mean(lonn).data
        else:
            continue

        if '_FillValue' in ds[varn].encoding.keys():
            fill_value = ds[varn].encoding['_FillValue']
            if fill_value != np.nan:
                mean = np.where(np.isnan(mean), fill_value, mean)
        ds_mean[varn].data = mean

    if was_da:
        return ds_mean[da_varn]
    return ds_mean


def save_compressed(ds, path, log=True):
    if log:
        logger.info('Save & compress file...')
    ds.to_netcdf(path)
    fs_org = os.path.getsize(path) / 1024.**2
    subprocess.run(['/usr/bin/nccopy', '-d9',
                    path, path.replace('.nc', '_temp.nc')])
    os.remove(path)
    os.rename(path.replace('.nc', '_temp.nc'), path)
    if log:
        fs_com = os.path.getsize(path) / 1024.**2
        logmsg = 'Save & compress file... DONE ({:.1f}MB -> {:.1f}MB)'.format(
            fs_org, fs_com)
        logger.info(logmsg)


def add_hist(ds, warning=True):
    """
    Add script name to the netCDF history attribute.

    Parameters:
    - ds (xarray.Dataset)

    Format if under Git control:
       'time /path/relative/to/repository/script_name.py (repository_name.git:
       revision tag' (revision hash as fallback)
    Else:
       'time /absolute/path/script_name.py'
    """
    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    filename = str(os.path.realpath(main.__file__))
    try:
        revision = subprocess.check_output([
            'git', 'describe', '--always']).strip().decode()
        rep_name = subprocess.check_output([
            'git', 'config', '--get', 'remote.origin.url']).strip().decode()
        branch_name = subprocess.check_output([
            'git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip().decode()
        filename_git = subprocess.check_output([
            'git', 'ls-tree', '--full-name',
            '--name-only', 'HEAD', filename]).strip().decode()
        if filename_git == '':
            if warning:
                logmsg = '{} is in a Git repository but not checked in!'.format(
                    filename)
                logger.warning(logmsg)
            str_add = '{} {}'.format(time, filename)
        else:
            str_add = '{} {}: {}, revision hash: {}, branch: {}'.format(
                time, rep_name, filename_git, revision, branch_name)
    except subprocess.CalledProcessError:
        if warning:
            logmsg = '{} is not under Git control!'.format(filename)
            logger.warning(logmsg)
        str_add = '{} {}'.format(time, filename)

    if 'history' in ds.attrs:
        ds.attrs['history'] = '{} {}'.format(ds.attrs['history'], str_add)
    else:
        ds.attrs['history'] = str_add


def select_region(da, region=None, country=None, mask_sea=False, average=False,
                  grid_point=None, return_mask=False):
    """Select a SREX region, country, or land mass my masking not-matching
    grid points. Note that region, country and maks_sea can be combined.
    Uses the regionmask package. Alternatively, select the nearest
    grid point to given coordinates. Can also average over the selected region.

    Parameters:
    - da (DataArray): A xarray.DataArray object of a variable depending on
      latitude and longitude. Can also be a xarray.Dataset object if it
      contains only one not-coordinate variable.
    - region ([list of str | str], optional): Region to select. If given
      has to be at least one of: ALA CGI WNA CNA ENA CAM AMZ NEB WSA SSA NEU
      CEU MED SAH WAF EAF SAF NAS WAS CAS TIB EAS SAS SEA NAU SAU
    - country ([list of str | str], optional): Country to select. See
      regionmask.defined_regions.natural_earth.countries_110[.names] for a list
      of valid country names.
    - mask_sea=False (bool, optional): If true the ocean will be masked.
    - average=False (bool, optional): If true the average over the regions
      will be taken.
    - grid_point (tuple, optional): tuple containing latitude and longitude
      and optionally an identifier string (lat, lon[, str]). Should not be
      combined with any other kwarg!
    - return_mask=False (bool, optional): If True return the mask instead of
      applying it.

    Returns:
    same type as input with regions selected/averaged

    TODO: test
    """
    input_ds = False
    if isinstance(da, xr.core.dataset.Dataset):
        varn = list(set(da.variables).difference(da.coords))
        if len(varn) == 1:
            attrs = da.attrs
            da = da[varn[0]]
            input_ds = True
        else:
            errmsg = 'Could not determine variable! da should be DataArray!'
            logger.error(errmsg)
            raise IOError(errmsg)

    mask_region = False
    if region is not None:
        if isinstance(region, str):
            region = [region]
        logger.debug('Get mask...')
        mask = regionmask.defined_regions.srex.mask(da, wrap_lon=True)
        logger.debug('Get mask... DONE')
        logger.debug('Get index...')
        idxs = regionmask.defined_regions.srex.map_keys(region)
        logger.debug('Get index... DONE')
        logger.debug('Combine indices...')
        mask_region = np.sum([mask == idx for idx in idxs], axis=0, dtype=bool)
        logger.debug('Combine indices... DONE')
        logger.debug('Select...')
        # da = da.where(mask_region)
        da.attrs['region'] = ', '.join(region)
        logger.debug('Select... DONE')

    mask_country = False
    if country is not None:
        if isinstance(country, str):
            country = [country]
        logger.debug('Get mask...')
        mask = regionmask.defined_regions.natural_earth.countries_50.mask(
            da, wrap_lon=True)
        logger.debug('Get mask... DONE')
        logger.debug('Get index...')
        idxs = regionmask.defined_regions.natural_earth.countries_50.map_keys(
            country)
        logger.debug('Get index... DONE')
        logger.debug('Combine indices...')
        mask_country = np.sum([mask == idx for idx in idxs], axis=0, dtype=bool)
        logger.debug('Combine indices... DONE')
        logger.debug('Select...')
        # da = da.where(mask_country)
        da.attrs['country'] = ', '.join(country)
        logger.debug('Select... DONE')

    mask_sea = False
    if mask_sea:
        mask = regionmask.defined_regions.natural_earth.land_110.mask(da) == 1
        mask_sea = ~mask
        # da = da.where(mask_sea)
        da.attrs['masked_sea'] = 'True'

    mask = np.ma.mask_or(np.ma.mask_or(mask_region, mask_country), mask_sea)
    if np.all(mask):
        logmsg = 'All grid points masked. Wrong combination of masks?'
        logger.warning(logmsg)

    if return_mask:
        return mask

    if np.any(mask):
        da = da.where(mask)

    if grid_point is not None:
        idx_lat = np.argmin(da['lat'].data - grid_point[0])
        idx_lon = np.argmin(da['lon'].data - grid_point[1])
        da = da.isel(lat=idx_lat).isel(lon=idx_lon)
        if len(grid_point) == 3:
            da.attrs['location'] = str(grid_point[2])

    if average and grid_point is None:
        da = area_weighted_mean(da)

    if input_ds:
        ds = da.to_dataset()
        ds.attrs = attrs
        return ds
    return da


class SelectRegion(object):
    """A convenience function to speed up select_region. Since the creation
    can take a long time this class caches it. Should only be used if the
    input DataArrays/Datasets are on the same grid!"""

    def __init__(self, da=None, region=None, country=None, mask_sea=False):
        """See region_mask for a full docstring. The 'da' parameter is here
        only used for mask creation."""
        self._region = region
        self._country = country
        self._mask_sea = False
        self.mask = None

        if da is not None:
            da, _, _ = self._get_da(da)
            self._get_mask(da)

    def _get_da(self, da):
        """Return a DataArray from a Dataset if possible"""
        input_ds = False
        attrs = None
        if isinstance(da, xr.core.dataset.Dataset):
            varn = list(set(da.variables).difference(da.coords))
            if len(varn) == 1:
                attrs = da.attrs
                da = da[varn[0]]
                input_ds = True
            else:
                errmsg = 'Could not determine variable! da should be DataArray!'
                logger.error(errmsg)
                raise IOError(errmsg)
        return da, input_ds, attrs

    def _get_mask(self, da):
        """Return the mask"""
        self.mask = select_region(
            da, region=self._region, country=self._country,
            mask_sea=self._mask_sea, return_mask=True)
        self.lat, self.lon = da['lat'].data, da['lon'].data

    def select_region(self, da, force=False, average=False):
        """See region_mask for a full docstring. If 'force' is True skip
        consistency tests."""
        da, input_ds, attrs = self._get_da(da)
        if self.mask is None:
            self._get_mask(da)
        else:
            logger.debug('Mask already defined, proceeding.')
            if not force:
                assert np.all(self.lat == da['lat'].data), 'Latitude not matching!'
                assert np.all(self.lon == da['lon'].data), 'Longitude not matching!'

        if np.any(self.mask):
            da = da.where(self.mask)

        if average:
            da = area_weighted_mean(da)

        if input_ds:
            ds = da.to_dataset()
            ds.attrs = attrs
            return ds
        return da
